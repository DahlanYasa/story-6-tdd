$("#darkmode-switch").on("click", () => {
    if ($("#darkmode-switch").is(":checked")) {
        // $(":root").css("--bg-color", "black");
        $("body").css("background-color", "black");

        $(".text-bg-color").css("background-color", "black");
        $(".text-bg-color").css("color", "white");
        $("#day-night-txt").text("Night Mode");

        // $("#navbar-switch").addClass("night-color");
        $("#navbar-color").css("background-color", "#333B43");
        $("#navbar-title").addClass("crimson-txt");
        $(".navbar-text").css("color", "#E9F4FF");

        $(".accord-header").addClass("crimson-bg");
    } else {
        // $(":root").css("--bg-color", "white");
        $("body").css("background-color", "white");

        $(".text-bg-color").css("background-color", "white");
        $(".text-bg-color").css("color", "black");
        $("#day-night-txt").text("Day Mode");

        // $("#navbar-switch").removeClass("night-color");
        $("#navbar-color").css("background-color", "#E9F4FF");
        $("#navbar-title").removeClass("crimson-txt");
        $(".navbar-text").css("color", "#333B43");

        $(".accord-header").removeClass("crimson-bg");
    }
});

$( () => {
    $( "#accordion" ).accordion({active: false, collapsible: true});
});