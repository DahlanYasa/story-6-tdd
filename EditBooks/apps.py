from django.apps import AppConfig


class EditbooksConfig(AppConfig):
    name = 'EditBooks'
