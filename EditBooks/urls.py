from django.urls import path
from . import views

app_name = 'EditBooks'

urlpatterns = [
    path('', views.searchBooks, name='searchBooks'),
    path('cari_buku/<str:param>/', views.cari_buku, name="cari_buku"),
]
