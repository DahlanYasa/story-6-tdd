from django.test import TestCase, Client
from django.urls import resolve
from .views import searchBooks

# Create your tests here.
# Create your tests here.
class BooksTest(TestCase):
    def test_books_is_exist(self):
        response = Client().get('/EditBooks/')
        self.assertEqual(response.status_code, 200)

    def test_books_using_index_func(self):
        found = resolve('/EditBooks/')
        self.assertEqual(found.func, searchBooks)
        self.assertEqual

    def test_books_using_index_template(self):
        response = Client().get('/EditBooks/')
        self.assertTemplateUsed(response, 'books.html')
