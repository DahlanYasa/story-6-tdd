var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

$(document).ready(function() {
  $("#change-theme").click(function() {
    var body_color = $('body').css('color');
    if (body_color == "rgb(202, 202, 202)") {
      console.log("aam");
      $('body').css("color", "white");
      $('body').css("background", "rgb(0, 54, 61)");
      $(".accordion").css("background-color", "#fb5b5a");
      $(".accordion").hover(function(){
        $(this).css("background-color", "#ff818a");
      }, function(){
        $(this).css("background-color", "#fb5b5a");
      });
    }
    else if (body_color == "rgb(255, 255, 255)") {
      $('body').css("color", "rgb(202, 202, 202)");
      $('body').css("background", "#472b62");
      $(".accordion").css("background-color", "#bc4873");
      $(".accordion").hover(function(){
        $(this).css("background-color", "#ec7da6");
      }, function(){
        $(this).css("background-color", "#bc4873");
      });
      console.log(body_color);
    }
  })
});