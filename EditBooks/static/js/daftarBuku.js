window.addEventListener("DOMContentLoaded", function (event) {
    if (localStorage.length != 0) {
        let content = [];
        Object.keys(localStorage).forEach(function (key) {
            let item = JSON.parse(localStorage.getItem(key));
            content.push(`<tr> 
            ${item.volumeInfo.imageLinks ? `<td><img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 18rem" alt="..."></td>`}
            <td><h5>${item.volumeInfo.title}</h5></td>
            <td><p><small class="text-muted">${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p></td>
            <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">See description</a></td>
            </tr>`)
        });
        document.querySelector(".body").innerHTML = '<div class="card-columns">' + content.join(" ") + '</div>';
    }
})

document.getElementById("searchbox").addEventListener("submit", function (event) {
    event.preventDefault();
    document.querySelector(".body").innerHTML = '<div class="loader"></div>'
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            localStorage.clear()
            if (JSON.parse(this.response).totalItems == 0) {
                document.querySelector(".body").innerHTML = "None";
            } else {
                document.querySelector(".body").innerHTML = '<div class="card-columns">' + JSON.parse(this.response).items.map((item, index, itemsArray) => {
                    localStorage.setItem(item.id, JSON.stringify(item));
                    return `<tr> 
                        ${item.volumeInfo.imageLinks ? `<td><img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" style="max-width: 18rem" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" style="max-width: 18rem" alt="..."></td>`}
                        <td><h5>${item.volumeInfo.title}</h5></td>
                        <td><p><small class="text-muted">${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p></td>
                        <td><a class="btn btn-dark" href="https://books.google.co.id/books?id=${item.id}">See description</a></td>
                        </tr>`;
                }).join(" ");
            }
        };
    };
    param = document.getElementById("id_search").value.trim().split(" ").join("_");
    xhttp.open("GET", `cari_buku/${param}/`, true);
    xhttp.setRequestHeader("X-CSRFToken", document.getElementsByName('csrfmiddlewaretoken')[0].value);
    // console.log(document.getElementsByName('csrfmiddlewaretoken')[0].value)
    xhttp.send();
})

