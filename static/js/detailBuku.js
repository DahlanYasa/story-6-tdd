if (localStorage.getItem(window.location.pathname.split('/')[2])) {
    let item = JSON.parse(localStorage.getItem(window.location.pathname.split('/')[2]))
    document.querySelector(".konten").innerHTML = `<div class="row my-5">
    <div class="col-md-4 py-3">
    ${ item.volumeInfo.imageLinks ? `<img src="${item.volumeInfo.imageLinks.thumbnail}" class="card-img-top" alt="...">` : `<img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" class="card-img-top" alt="...">`}
    </div>
    <div class="col-md-8 px-3">
        <div class="row">
            <h2 class="card-title px-3">${item.volumeInfo.title}</h1>
            <p class="card-text"><small class="text-muted px-3">Author(s): ${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p>
        </div>
        <div class="row px-3">
            <p>${item.volumeInfo.description}</p>
        </div>
    </div>
    </div>`
}


function back() {
    window.history.back();
}