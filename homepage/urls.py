from django.urls import path
from homepage.views import landing_page, index_page

app_name = 'homepage'

urlpatterns = [
    path('', index_page, name='index'),
    path("homepage/", landing_page, name="homepage"),
]
