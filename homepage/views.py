from django.shortcuts import render, redirect
from homepage.models import Status
from homepage.forms import Status_Form
# Create your views here.

def index_page(request):
    return redirect('/homepage/')

def landing_page(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('/homepage/')

    statuses = Status.objects.all()
    context = {
        'page_title':'landing',
        'status':statuses,
        'form':form,
    }
    return render(request, 'index.html', context)
